const BASE_URL = "https://63f6d7d0ab76703b15c54968.mockapi.io";
//63f6d7d0ab76703b15c54968.mockapi.io/sv"

function fetchDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      console.log(`  🚀: res`, res);
      var dssv = res.data;
      tatLoading();
      renderDSSV(dssv);
    })
    .catch(function (err) {
      tatLoading();
      console.log(`  🚀: err`, err);
    });
}

fetchDSSV();

function renderDSSV(dssv) {
  var contentHTML = "";
  for (var i = dssv.length - 1; i >= 0; i--) {
    var item = dssv[i];
    var contentTr = `
    <tr>
        <td> ${item.ma} </td>
        <td> ${item.ten} </td>
        <td> ${item.email} </td>
        <td> 0 </td>
        <td>
             <button 
             onclick="xoaSV('${item.ma}')"
             class="btn btn-danger">
             Xoá
             </button>
               <button 
             onclick="suaSV('${item.ma}')"
             class="btn btn-warning">
             Sửa
             </button>
        </td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function xoaSV(id) {
  console.log(`  🚀: xoaSV -> id`, id);
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchDSSV();
      console.log(`  🚀: xoaSV -> res`, res);
    })
    .catch(function (err) {
      console.log(`  🚀: xoaSV -> err`, err);
    });
}
function suaSV(id) {
  console.log(`  🚀: suaSV -> id`, id);
  //   get detail by id
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log("detail sv theo id", id, res);
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function capNhatSV() {
  var sv = layThongTinTuForm();
  console.log(`  🚀: capNhatSV -> sv`, sv);
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      console.log(res);
      // lấy dữ liệu mới nhất từ server sau khi update thành công ( update trên server )
      fetchDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}
// edit sv : api detail, api update

function themSV() {
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      // lấy data mới nhất từ server sau khi thêm thành công ( thêm trên server)
      fetchDSSV();
    })
    .catch(function (err) {
      console.log(`  🚀: themSV -> err`, err);
    });
}
